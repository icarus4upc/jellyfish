CC = g++
RM = rm -rf
CFLAGS = -c -Wall -std=c++11 -I. -g
LDFLAGS = -llcm -lpthread -lglib-2.0 -ltiff -lgeotiff -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs -lopencv_calib3d -lopencv_stitching

OBJ = ErosionDilation.o functions.o HotSpot.o jelly.o
EXE = jellyfish

# All Targets
all: $(EXE)

# One Target
jellyfish: $(OBJ) 
	$(CC) -o $@ $^ $(LDFLAGS)

.cpp.o: 
	$(CC) $(CFLAGS) $< -o $@

# Other Targets
clean:
	$(RM) $(RTDP_OBJS) *.o *~ 

clean-all:
	$(RM) $(RTDP_OBJS) *.o *~ $(EXE)

