//============================================================================
// Name        : jelly.cpp
// Author      : Javier Fuentes / Cristina Barrado
// Version     : 0.1
// Copyright   : LGPL
// Description : Detection of Jellyfish
//============================================================================

#include <iostream>
#include <list>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "functions.hpp"

using namespace cv;
using namespace std;

// GLOBALS
vector<string> inputNames;
//default RBG thresholds
unsigned  RMIN=120;
unsigned  RMAX=255;
unsigned  GMIN=0;
unsigned  GMAX=55;
unsigned  BMIN=100;
unsigned  BMAX=255;
bool markJelly=true;
bool patternMatching=false;
string outputFormat="jpeg";
string outputPath=".";
string filterJelly="ellipse";

//////////////////////////////////////////////////////

	void printUsage()
	{
	    cout << endl;
	    cout << "Jellyfish - Detects Jellyfish in a visual image" << endl;
	    cout << endl;
	    cout << "Usage: jelly [img1 img2 ... imgN] [Options]" << endl;
	    cout << endl;
	    cout << "Options:" << endl;
	    cout << endl;
	    cout << "  -t values ..., --threshold values ..." << endl;
	    cout << "      range of RBG values: Rmin-Rmax Gmin-Gmax Bmin-Bmax (default is ";
	    cout << RMIN << "-" << RMAX << ", " << GMIN << "-" << GMAX << ", " << BMIN << "-" << BMAX << ")" << endl;
	    cout << endl;
	    cout << "  -m (yes|no), --mark (yes|no)" << endl;
	    cout << "      mark jellyfish in the image (default is " << (markJelly? "yes": "no") << ")" << endl;
	    cout << endl;
	    cout << "  -f (jpeg|png|bmp|tiff), --output_format (jpeg|png|bmp|tiff)" << endl;
	    cout << "      converted image output format (default is " << outputFormat << ")" << endl;
	    cout << endl;
	    cout << "  -p path, --output_path path" << endl;
	    cout << "      output path (default is .)" << endl;
	    cout << endl;
	    cout << "  -k (disk|ellipse), --kernel_filter (disk|ellipse)" << endl;
	    cout << "      set erode/dilate kernel filter (default is " << filterJelly << ")" << endl;
	    cout << endl;
	    cout << "  -r , --pattern_matching " << endl;
	    cout << "      to activate pattern matching filtering (default is no pattern matching)" << endl;
	    cout << endl;
	    cout << "  -h ,--help" << endl;
	    cout << "      print usage" << endl;
	    cout << endl;
}


// Parse command line arguments
int parseArguments(int argc, char* argv[])
{
	if (argc == 1)
	{
	    return -1;
	}

	for (int i = 1; i < argc; i++)
	{
	    if (string(argv[i]) == "--help" || string(argv[i]) == "-h" || string(argv[i]) == "/?")
	    {
	        return -1;
	    }
	    else if (string(argv[i]) == "--threshold" || string(argv[i]) == "-t")
	    {
	    	RMIN = atof(argv[i+1]); RMAX = atof(argv[i+2]);
	    	GMIN = atof(argv[i+3]); GMAX = atof(argv[i+4]);
	    	BMIN = atof(argv[i+5]); BMAX = atof(argv[i+6]);
	    	i+=6;
	    }
	    else if (string(argv[i]) == "--mark" || string(argv[i]) == "-m")
	    {
	    	if (string(argv[i+1]) == "yes")
	    	{
	    		markJelly = true;
	    	}
	    	else if (string(argv[i+1]) == "no")
	    	{
	    		markJelly = false;
	    	}
	    	else
	    	{
	    	    return -1;
	    	}
	    	i++;
	    }
	    else if (string(argv[i]) == "--output_format" || string(argv[i]) == "-f")
	    {
	    	outputFormat = argv[i+1];
	    	i++;
	    	if (outputFormat != "jpeg" && outputFormat != "bmp" && outputFormat != "png" && outputFormat != "tiff")
	    	{
	    	    return -1;
	    	}
	    }
	    else if (string(argv[i]) == "--output_path" || string(argv[i]) == "-p")
	    {
	    	outputPath = argv[i+1];
	    	i++;
	    }
	    else if (string(argv[i]) == "--kernel_filter" || string(argv[i]) == "-k")
	   	{
	   	    	filterJelly = argv[i+1];
	   	    	i++;
	   	    	if (filterJelly != "disk" && filterJelly != "ellipse")
	   	    	{
	   	    	    return -1;
	   	    	}
	   	}
	    else if (string(argv[i]) == "--pattern_matching" || string(argv[i]) == "-r")
		{
	    	patternMatching = true;
		}
        else
        {
        	if (argv[i][0] == '-')
	    	{
	    	    return -1;
	    	}
            inputNames.push_back(argv[i]);
        }
    }

    if (inputNames.size() < 1)
	{
	    return -1;
	}

    return 0;
}

int main ( int argc, char** argv )
{
  cout << "Welcome to OpenCV!!!" << endl;
  cout << "Executing ";
  for (int i = 0; i < argc; i++){
	  cout << argv[i] << " ";
  }
  cout << endl;

  // Parse command line arguments
  int err = parseArguments(argc, argv);
  if (err != 0) {
		printUsage();
		exit (-1);
  }

  for (int i = 0; i < (int)inputNames.size(); i++){
	  string name = getRawBasename(inputNames[i]);
	  cout << "Processing " << name << endl;

	  Mat image;
	  image=imread(inputNames[i]);
	  CV_Assert(!image.empty());
	  //namedWindow( "Jellyfish Original", CV_WINDOW_NORMAL );
	  //imshow( "Jellyfish Original", image );

	  cout << "... color enhancing" << endl;
	  // processing the image
	  Mat image1(image.rows, image.cols, CV_8UC3);
	  image1=color_enhancement(image, name);
	  //namedWindow( "Jellyfish Processed Image 1", CV_WINDOW_AUTOSIZE );
	  //imshow( "Jellyfish Processed Image 1", image1 );
	  //imwrite( "my"+name+"_f2_after_color.jpg", image1 );

	  cout << "... segmentation" << endl;
	  Mat image2;
	  image2=segmentation(image1, name, Scalar(BMIN, GMIN, RMIN), Scalar(BMAX,GMAX,RMAX), filterJelly=="ellipse");
	  //namedWindow( "Jellyfish Processed Image 2", CV_WINDOW_AUTOSIZE );
	  //imshow( "Jellyfish Processed Image 2", image2 );
	  //imwrite( "my"+name+"_f2_segments.jpg", image2 );

	  //discard_date(image2);
	  //imwrite( "my"+name+"_f2_segments_nolabel.jpg", image2 );

	  cout << "... searching candidates ..." ;
	  list<HotSpot*>* jc = jellyfish_candidates(image2);
	  cout << "number of jelly candidates is " << jc->size() << endl;

	  if (patternMatching){
		  cout << "... checking candidates ..." ;
		  check_candidates(image, jc);
		  cout << "number of jelly reduced to " << jc->size() << endl;
	  }

	  if (markJelly){
		  paint_jellyfish(image, jc);
		  //imwrite( "my"+name+"_f4_last.jpg", image);
		  writeImage(image, outputPath+"/"+name+"_f4_last", "", outputFormat );
		  //namedWindow( "Jellyfish search", CV_WINDOW_AUTOSIZE );
		  //imshow( "Jellyfish search", image );
	  }
  }

  cout << "See you back to OpenCV!!!" << endl;
  //waitKey(0);
  return 0;
}
