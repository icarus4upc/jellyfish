This Software is provided with no warranties, and under same license than OpenCV 3.2, the [BSD (3-clause)](https://github.com/opencv/opencv/blob/4.4.0/LICENSE)

By downloading, copying, installing or using the software you agree to this license. If you do not agree to this license, do not download, install, copy or use the software.

---

**Dependencies**

- C++ development enviroment
- OpenCV 3.2
- LCM - Lightweight Communications and Marshalling available at lcm-proj.github.io
- GeoTIFF library 1.5

---

## Build and Execute

Build the binaries from terminal using:

$ make

Run the code using

$ jellyfish \[img1 img2 ... imgN\] \[Options\]

Option -h to see the available options


---

## Reference

BibTex to reference this work:

@inproceedings{barrado2014jellyfish,
  title={Jellyfish monitoring on coastlines using remote piloted aircraft},
  author={Barrado, C and Fuentes, Javier A and Salam{\'\i}, E and Royo, P and Olariaga, Alejandro D and L{\'o}pez, J and Fuentes, Ver{\'o}nica L and Gili, Jose Maria and Pastor, E},
  booktitle={IOP Conference Series: Earth and Environmental Science},
  volume={17},
  number={1},
  pages={012195},
  year={2014},
  organization={IOP Publishing}
}
---

## Contact

Further contact: cristina(dot)barrado(at)upc(dot)edu