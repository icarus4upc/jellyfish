/**
* @file extraction from Morphology_1.cpp
* @brief Erosion and Dilation sample code
* @author OpenCV team
*/

#include "ErosionDilation.hpp"
#include "functions.hpp"

using namespace cv;

int const max_elem = 2;
int const max_kernel_size = 21;

/**
* @function Erosion
*/
void Erosion(Mat src, Mat& erosion_dst, int mask)
{
	Mat element;
	if (mask==0){
		// Matlab was strel('disk',2) = {0,0,1,0,0,/ 0,1,1,1,0,/ 1,1,1,1,1,/ 0,1,1,1,0,/ 0,0,1,0,0};
		unsigned char kernel[]={0,0,1,0,0, 0,1,1,1,0, 1,1,1,1,1, 0,1,1,1,0, 0,0,1,0,0};
		element=Mat(5,5, CV_8UC1, kernel);
		//show_mat(element,5);
	} else {
		int erosion_type = MORPH_ELLIPSE; //also _RECT and _CROSS
		int erosion_size = 2;

		element = getStructuringElement(erosion_type,
			Size(2 * erosion_size + 1, 2 * erosion_size + 1),
			Point(erosion_size, erosion_size));
	}

	/// Apply the erosion operation
	erode(src, erosion_dst, element);
	//imshow("Erosion Demo", erosion_dst);
}

/**
* @function Dilation
*/
void Dilation(Mat src, Mat& dilation_dst, int mask)
{
	Mat element;
	int dilation_type;
	int dilation_size;

	if (mask==0){
		// Matlab was strel('disk',3) = 5x5 with 1's
		dilation_type = MORPH_RECT;
		dilation_size = 2;
	} else {
		dilation_type = MORPH_ELLIPSE;
		dilation_size = 3;
	}
	element = getStructuringElement(dilation_type,
		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		Point(dilation_size, dilation_size));
	//show_mat(element,5);

	/// Apply the dilation operation
	dilate(src, dilation_dst, element);
	//imshow("Dilation Demo", dilation_dst);
}
