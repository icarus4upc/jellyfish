/*
 * functions.cpp
 *
 *  Created on: Jun 19, 2014
 *      Author: cristina
 */

#include "functions.hpp"

Mat color_separate (Mat a){
	int n_pixels=a.rows*a.cols;
	//cout << "in A(0,0)= " << (int)a.at<Vec3b>(0,0)[0] << ", " << (int)a.at<Vec3b>(0,0)[1]<< ", " << (int)a.at<Vec3b>(0,0)[2] << endl;

	Mat af(a.size(),CV_64F);
	a.convertTo(af, CV_64F, 1.0/255);
	//cout << "AF(0,0)= " << af.at<Vec3d>(0,0)[0] << ", " << af.at<Vec3d>(0,0)[1]<< ", " << af.at<Vec3d>(0,0)[2] << endl;

	Scalar mu, sigma;
	meanStdDev(af, mu, sigma);

	double mdata[3]={mu[0], mu[1], mu[2]};
	Mat M(1,3, CV_64F, mdata);
	//cout << "Mean=" << M << endl;

	Mat b(n_pixels, 3, CV_64F, af.data);
	//Mat b=Mat(n_pixels, 3, CV_64F, af.data).clone();
	//cout << "B=" << b.channels() << "channels and " << b.size() << endl;

	Mat t(3,3,CV_64F);
	mulTransposed(b, t, true,noArray());
	//cout << "B'B=" << t << endl;
	Mat t2(3,3,CV_64F);;
	mulTransposed(M,t2, true,noArray(),n_pixels);
	//cout << "xM'M=" << t2 << endl;

	Mat Cov(3,3,CV_64F);
//		calcCovarMatrix(b,Cov,M,CV_COVAR_NORMAL|CV_COVAR_ROWS,CV_64F);
//		cout << "xxMean=" << M << endl;
//		cout << "xxCov=" << Cov/(n_pixels-1) << endl;
	Cov=(t-t2)/(n_pixels-1);
	//cout << "Cov=" << Cov << endl;
	//cout << "  is " <<  Cov.channels() << " channels and " << Cov.size() << endl;
	//cout << "Cov(0,0)=" << Cov.at<double>(0,0) << endl;

	double ddata[9]={Cov.at<double>(0,0),0,0,
			0,Cov.at<double>(1,1), 0,
			0,0,Cov.at<double>(2,2)};
	for (int i=0; i<3; i++) ddata[i*3+i]=sqrt(ddata[i*3+i]);
	Mat s(3,3, CV_64F, ddata); //s=sqrt(Cov.diag(0));
	//cout << "S=" << s << endl;
	//cout <<  "  S is" << s.channels() << "channels and " << s.size() << endl;

	Mat i_s(3,3, CV_64F);
	i_s=s.inv(DECOMP_SVD);
	//cout << "S-inv=" << i_s << endl;

	Mat Corr(3,3,CV_64F);
	//Corr=i_s*Cov; //gemm(i_s,Cov,1,noArray(), 0, Corr,0);
	//Corr=Corr*i_s; //gemm(Corr,i_s,1,noArray(), 0,Corr,0);
	Corr=i_s*Cov*i_s;
	completeSymm(Corr);
	for (int i=0; i<3; i++) Corr.at<double>(i,i)=1;
	//cout << "Corr=" << Corr << endl;
	//cout <<  "  Corr is " << Corr.channels() << " channels and " << Corr.size() << endl;

	Mat V(3,3,CV_64F),D(3,1,CV_64F);
	eigen(Corr, D, V);
	//cout << "V=" << V << endl;
	//cout << "D=" << D << endl;

	double wdata[9]={D.at<double>(0),0,0,
				0,D.at<double>(1), 0,
				0,0,D.at<double>(2)};
	for (int i=0; i<3; i++) wdata[i*3+i]=(wdata[i*3+i]<0?0.0:sqrt(1.0/wdata[i*3+i]));
	Mat W(3,3,CV_64F,wdata); // W=diagMatrix(sqrt(1/d_i))
	//cout << "W=srqt(D_inv)=" << W << endl;

	Mat T(3,3,CV_64F);
	T=i_s*V.t()*W*V*s;
	//cout << "T = inv_S x eingV x sqrt(inv(eigD)) x eigV' x S = " << endl << T << endl ;

	Mat TCT(3,3,CV_64F);
	TCT=T.t()*Cov*T;
	//cout << "TCT= " << TCT << endl;

	double Tdata[9]={TCT.at<double>(0,0),0,0,
					0,TCT.at<double>(1,1), 0,
					0,0,TCT.at<double>(2,2)};
	for (int i=0; i<3; i++) Tdata[i*3+i]=(Tdata[i*3+i]<0?0.0:1.0/sqrt(Tdata[i*3+i]));
	Mat iTCT(3,3,CV_64F,Tdata);

	T=T*iTCT*s;
	//cout << "T= " << T << endl;

	Mat Offset(1,3,CV_64F);;
	Offset=M - M*T;
	//cout << "Offset= " << Offset << endl;

	b=b*T;
	//cout << "After xT= " << b.at<double>(0,0) << ", " << b.at<double>(0,1)<< ", " << b.at<double>(0,2) << endl;

	for (int i=0; i<b.cols; i++)
		b.col(i)=b.col(i)+Offset.at<double>(i);
	//cout << "After +Off= " << b.at<double>(0,0) << ", " << b.at<double>(0,1)<< ", " << b.at<double>(0,2) << endl;

	//RETURN
	Mat b3c=Mat(a.rows, a.cols, CV_64FC3, b.data);
	Mat r;
	b3c.convertTo(r, CV_8UC3, 255.0);
	//cout << "out A(0,0)= " << (int)r.at<Vec3b>(0,0)[0] << ", " << (int)r.at<Vec3b>(0,0)[1] << ", " << (int)r.at<Vec3b>(0,0)[2] << endl;
	return (r);
}

Mat color_normalize(Mat a){
	Mat af;
	a.convertTo(af, CV_64F, 1./255);
	//cout << "AF(0,0)= " << af.at<Vec3d>(0,0)[0] << ", " << af.at<Vec3d>(0,0)[1]<< ", " << af.at<Vec3d>(0,0)[2] << endl;

	// apply the intrans-stretch to increase contrast
	double eps = std::numeric_limits<double>::epsilon();
	Scalar medias = mean(af);
	double media = (medias[0]+medias[1]+medias[2])/3.0;
	//cout << "Mean=" << media;

	// Expende los valores de los pixels de 0-1 segun sean < o > de la media total
//	double * p = (double *)af.data;
//	for (int i = 0; i < af.rows*af.cols*af.channels(); i++)
//		*p++ = 1. / (1 + sqrt(media / ((*p) + eps)));
//	//cout << "out A(0,0)= " << af.at<Vec3d>(0,0)[0] << ", " << af.at<Vec3d>(0,0)[1] << ", " << af.at<Vec3d>(0,0)[2] << endl;
	for (int i=0; i<af.rows; i++)
		for (int j=0; j<af.cols; j++)
			for (int b=0; b<3; b++)
				af.at<Vec3d>(i,j)[b]=1. / (1 + sqrt(media / (af.at<Vec3d>(i,j)[b] + eps)));

	af.convertTo(a, CV_8UC3, 255.0);
	//cout << "out A(0,0)= " << (int)a.at<Vec3b>(0,0)[0] << ", " << (int)a.at<Vec3b>(0,0)[1] << ", " << (int)a.at<Vec3b>(0,0)[2] << endl;
	return a;
}

Mat color_enhancement (Mat a, String name){
	Mat a2=color_separate(a);
	/*imwrite( "my"+name+"_f1_sep_bands.jpg", a2 );*/
	a=color_normalize(a2);

	return a;
}

/*
 * @function segmentation
 * input: Mat a is the collored matrix to segment
 * input: name is the basefile name for debug output
 * input: rgb_min and rgb_max are the RGB threshold limits of the segmentation
 * input: filter is 0 for disk/MatLab filtering and 1 for ellipse/OpenCV
 */
Mat segmentation(Mat a, String name, Scalar rgb_min, Scalar rgb_max, int filter){
	/* SEE alternative with inRange !!!
	vector<Mat> rgb;
	split(a, rgb); // 0=b, 1=g 2=r
	//cout << "rgb de A(0,0)= " << (int)rgb[0].at<uchar>(0,0) << ", " << (int)rgb[1].at<uchar>(0,0) << ", " << (int)rgb[2].at<uchar>(0,0) << endl;

	Mat rt,gt,bt;
	threshold(rgb[0],rt,80,true,1); //inverted type threshold
	threshold(rgb[1],gt,70,true,1);
	threshold(rgb[2],bt,80,true,1);
	//cout << "threshold de A(0,0)= " << (int)rt.at<bool>(0,0) << ", " << (int)gt.at<bool>(0,0) << ", " << (int)bt.at<bool>(0,0) << endl;

//	Mat masc = Mat::zeros(a.size(), CV_8UC1); // this is a nrows x ncols boolean matriz
//	masc= (rgb[0] < 80) | (rgb[1] < 70) | (rgb[2] < 80) ;
	Mat masc;
	bitwise_or(bt, gt, masc);
	bitwise_or(masc,rt, masc);
	//cout << "bool(0,0)= " << (int)masc.at<bool>(0,0) << endl;
	 */
	Mat masc;
	//inRange(a, Scalar(80, 70,80), Scalar(255,255,255), masc);
	inRange(a, rgb_min, rgb_max, masc); // segun valores sacados por GIMP para medusas R[140]G[0-6-50]B[127-160] -->reflejos no tienen casi B
	//masc=abs(masc-255);
	//imwrite( "my"+name+"_f2_after_inRagne.jpg", masc );

	Mat temp_masc=Mat::zeros(a.size(), CV_8UC1);
	// erode
	Erosion(masc, temp_masc, filter);
	// dilate
	Dilation(temp_masc, masc, filter);
	//imwrite( "my"+name+"_f2_after_erode+dilate.jpg", masc );

	Mat r;
	masc.convertTo(r, CV_8U, 255.0, 0);
	//cout << "r(hotspot)= " << (int)r.at<uchar>(2871,5) << "o" << (int)r.at<uchar>(5,2871)  << endl;

	return r;
}

void show_mat(Mat m, int s){
	cout << "(" << m.cols << " x " << m.rows << ")";
	switch (m.type()){
	case CV_8UC3:
		cout << " of type CV_8UC3"<< endl;
		for (int i=0; i<s; i++) {
			for (int j=0; j<s; j++){
				cout << "("<<(int)m.at<Vec3b>(i,j)[0] << " "<<(int)m.at<Vec3b>(i,j)[1] << " "<<(int)m.at<Vec3b>(i,j)[2] << ")  ";
			}
			cout << "..." << endl;
		}
		cout << "..." << endl;
		break;
	case CV_64FC3:
		cout << " of type CV_64FC3"<< endl;
		for (int i=0; i<s; i++) {
			for (int j=0; j<s; j++){
				cout << "("<<(double)m.at<Vec3d>(i,j)[0] << " "<<(double)m.at<Vec3d>(i,j)[1] << " "<<(double)m.at<Vec3d>(i,j)[2] << ")  ";
			}
			cout << "..." << endl;
		}
		cout << "..." << endl;
		break;
	case CV_8UC1:
		cout << " of type CV_8UC1"<< endl;
		for (int i=0; i<s; i++) {
			for (int j=0; j<s; j++){
				cout << "("<< (unsigned int) m.at<uchar>(i,j) << ")  ";
			}
			cout << "..." << endl;
		}
		cout << "..." << endl;
		break;
	case CV_64FC1:
		cout << " of type CV_64FC1"<< endl;
		for (int i=0; i<s; i++) {
			for (int j=0; j<s; j++){
				cout << "("<< m.at<double>(i,j) << ")  ";
			}
			cout << "..." << endl;
		}
		cout << "..." << endl;
		break;
	default:
		cout << "matrix type not considered in this function!!" << m.type() << endl;
	}
}

int writeImage(Mat img, string name, string suffix, string format)
{
	string imgName = name + suffix;
	if (format == "jpeg")
		imgName += ".JPG";
	else if (format == "bmp")
		imgName += ".BMP";
	else if (format == "png")
		imgName += ".PNG";
	else if (format == "tiff")
		imgName += ".TIF";

	try
	{
		if (!imwrite(imgName, img))
		{
			cout << "ERROR: writeImage - cannot write image " << imgName << endl;
			return -1;
		}
	}
	catch( cv::Exception& e )
	{
	    const char* errmsg = e.what();
	    cout << "ERROR: writeImage exception - " << errmsg << endl;
	    return -1;
	}

	return 0;
}


void discard_date(Mat& a){
	Mat date=a(Range(3230-1, 3230+19-1), Range(4709-1, 4709+164-1));
	date = date & Mat::zeros(19, 164, CV_8UC1);
}

// IE'09 segmentation algorithm to find hot spots
//  adapted from FPImage::Segmentate in HotSpots project
//	in matlab code is [img_label]=bwlabel(img_op,8); in segment.m !!!
list<HotSpot*>* jellyfish_candidates(Mat masc){
		list<HotSpot*>* hotSpots = new list<HotSpot*>();
		HotSpot* line[masc.cols];

		uchar* row_ptr = masc.ptr<uchar>(0);
		int x = 0, y = 0;
		uchar temp = row_ptr[x];

		if (temp == 0)
		{
			line[x] = NULL;
		}
		else
		{
			HotSpot* h = new HotSpot(x, y, temp);
			hotSpots->push_back(h);
			line[x] = h;
		}

		for (x = 1; x < masc.cols; x++)
		{
			temp = row_ptr[x];

			if (temp == 0)
			{
				line[x] = NULL;
			}
			else if (line[x - 1] == NULL)
			{
				HotSpot* h = new HotSpot(x, y, temp);
				hotSpots->push_back(h);
				line[x] = h;
			}
			else
			{
				line[x - 1]->AddPixel(x, y, temp);
				line[x] = line[x - 1];
			}
		}

		// process next lines
		for (y = 1; y < masc.rows; y++)
		{
			row_ptr = masc.ptr<uchar>(y);
			x = 0;
			temp = row_ptr[x];

			if (temp == 0)
			{
				line[x] = NULL;
			}
			else if (line[x] == NULL)
			{
				HotSpot* h = new HotSpot(x, y, temp);
				hotSpots->push_back(h);
				line[x] = h;
			}
			else
			{
				line[x]->AddPixel(x, y, temp);
			}

			for (x = 1; x < masc.cols; x++)
			{

				temp = row_ptr[x];

				if (temp == 0)
				{
					line[x] = NULL;
				}
				else if ((line[x] == NULL) && (line[x - 1] == NULL))
				{
					HotSpot* h = new HotSpot(x, y, temp);
					hotSpots->push_back(h);
					line[x] = h;
				}
				else if ((line[x] == NULL) && (line[x - 1] != NULL))
				{
					line[x - 1]->AddPixel(x, y, temp);
					line[x] = line[x - 1];
				}
				else if ((line[x] != NULL) && (line[x - 1] == NULL))
				{
					line[x]->AddPixel(x, y, temp);
				}
				else if (line[x] == line[x - 1])
				{
					line[x]->AddPixel(x, y, temp);
				}
				else
				{
					HotSpot* h = line[x - 1];
					line[x]->MergeHotSpot(h);
					line[x]->AddPixel(x, y, temp);
					for (int j = 0; j < masc.cols; j++)
					{
						if (line[j] == h) line[j] = line[x];
					}
					hotSpots->remove(h);
					delete h;
				}
			}
		}
		return hotSpots;
}

// build Features vector of a candidate jellyfish crop image c
void calculateH_feature(Mat c, double H[]){
    assert(!c.empty());
	//cout << " medusa  =" << endl;
	//show_mat(c, 5);

    Mat Box=Mat::zeros(SIZE_NORM+2,SIZE_NORM+2, c.type()); // creates a picture mark with 0s
	Mat nc=Box(Range(1,SIZE_NORM+1), Range(1,SIZE_NORM+1)); // point of interest=64x64
	resize(c, nc, cv::Size(SIZE_NORM,SIZE_NORM));
	//cout << " medusa normalizada+marco =" << endl;
	//show_mat(Box, 12);

	double kdatax[3]={-1, 0, 1};
	Mat kernelx(1,3, CV_64F, kdatax);
	double kdatay[3]={1, 0, -1};
	Mat kernely(3,1, CV_64F, kdatay);
	Mat Box_gradx;
	Mat Box_grady;
	filter2D(Box, Box_gradx, CV_64F, kernelx);
	filter2D(Box, Box_grady, CV_64F, kernely);
	//cout << "Grad X" ; 	show_mat(Box_gradx,12);
	//cout << "Grad Y" ; 	show_mat(Box_grady,12);
	Mat gradx=Box_gradx(Range(1,SIZE_NORM+1), Range(1,SIZE_NORM+1));
	Mat grady=Box_grady(Range(1,SIZE_NORM+1), Range(1,SIZE_NORM+1));

	Mat angles; //(64,64,c.type());
	Mat magnit; //(64,64,c.type());
	Mat roi_angles; //(18,18,c.type());
	Mat roi_mag; //(18,18,c.type());

	int b;  // color band
	int hi; // H index for color band b
	for (b=2, hi=0; b>=0; b--, hi++){ // First R, then G, final B
		//Mat gradx, grady, angles(1,64*64,c.type()), magnit(1,64*64,c.type());

		angles.create(SIZE_NORM,SIZE_NORM,CV_64F);
		magnit.create(SIZE_NORM,SIZE_NORM,CV_64F);

//		double* px = (double *)gradx_rgb[i].data;
//		double* py = (double *)grady_rgb[i].data;
//		double* pa = (double *)angles.data;
//		double* pm = (double *)magnit.data;
//		//cout << "ATAN2 test= "<< atan2(0,1)/M_PI*180<< " "<< atan2(1,0)/M_PI*180<< " "<< atan2(0,-1)/M_PI*180<< " "<< atan2(-1,0)/M_PI*180<< endl;
//		for (int ne=0; ne < SIZE_NORM*SIZE_NORM ; ne++ ){
		for (int r=0; r< gradx.rows; r++) for (int c=0; c<gradx.cols; c++){
			double x=(double)gradx.at<Vec3d>(r,c)[b];
			double y=(double)grady.at<Vec3d>(r,c)[b];
			angles.at<double>(r,c) = atan2( y, x ) ;
		    magnit.at<double>(r,c) = sqrt(y*y + x*x);
		}
	    assert((gradx.rows == magnit.rows) && (gradx.cols == angles.cols));
		//cout << "Angles" ; 	show_mat(angles,20);
		//cout << "Magnit" ; 	show_mat(magnit,20);

	    // make 6x6 9-overlapped 18x18 TILES of angles and magnit
		int cont=0;  // tile counter
		for (int sy=0; sy<6; sy++)
			for (int sx=0; sx<6; sx++){
				roi_angles=angles(Range(sy*9,sy*9+SIZE_TILE), Range(sx*9, sx*9+SIZE_TILE)).clone();
				roi_mag=magnit(Range(sy*9,sy*9+SIZE_TILE), Range(sx*9, sx*9+SIZE_TILE)).clone();
				//cout << "RoI angles" ; 	show_mat(roi_angles,18);
				//cout << "RoI mag" ; 	show_mat(roi_mag,18);

				int bin=cont*7+hi*SIZE_BOUND/2/3; // index in H for the 7 angles
				for (double ang_lim=-M_PI+2*M_PI/7; ang_lim<=M_PI; ang_lim+=2*M_PI/7, bin++){
					H[bin]=0.0;
					for (int roi_i=0; roi_i<SIZE_TILE; roi_i++) for (int roi_j=0; roi_j<SIZE_TILE; roi_j++)
						if (roi_angles.at<double>(roi_i, roi_j) < ang_lim){
							roi_angles.at<double>(roi_i, roi_j)=100; // to do not process it again
							H[bin]+=roi_mag.at<double>(roi_i, roi_j); // add magnitude to the bin
							//cout << "adding magnitude" << roi_mag.at<double>(roi_i, roi_j) <<" to position "<< bin+cont*7+hi*756/3<< endl;
						}
				}
				// normalize the 7 H-elements of the TILE
				double Hnorm=0;
				bin=cont*7+hi*SIZE_BOUND/2/3;
				for (int a=0; a<7; a++)
					Hnorm=Hnorm+H[bin+a]*H[bin+a];
				Hnorm=sqrt(Hnorm);
				for (int a=0; a<7; a++)
					H[bin+a]=H[bin+a]/(Hnorm+.01);

				//next tile
				cont++;
			}
	}
	//cout << "H=[" << H[0]<<", "<< H[1]<<", "<< H[2]<<", "<< H[3]<<", "<< H[4];
	//cout <<", "<< H[5]<<", "<< H[6]<<", "<< H[7]<<", "<< H[8]<<", "<< H[9]<<", "<< H[10]<< " ..." << endl;
	// Return normalised histogram
	//cout << "H norm [R,G,B] = " << H[172-1] << ", "<< H[424-1] << ", "<< H[710-1]<< endl;
}

void readModel(const char * filename, struct JellyModel& model){
	ifstream f;
	f.open(filename);
    assert(f.is_open());
    // alpha=1.3966 1.7276 1.7116
	for (int i=0; i<3; i++)
		f >> model.alpha[i];
	//model.dimension[3]={172,424,710};
	for (int i=0; i<3; i++)
		f >> model.dimension[i];
	//model.threshold[3]={0.8671,0.9891,0.0478};
	for (int i=0; i<3; i++)
		f >> model.threshold[i];
	//model.error[3]={0.0577,0.0577,0};
	for (int i=0; i<3; i++)
		f >> model.error[i];
	//model.boundary[1512];
	for (int i=0; i<SIZE_BOUND; i++)
		f >> model.boundary[i];
	//cout << "BOUNDARY left(min) [R,G,B] = " << model.boundary[172-1] << ", "<< model.boundary[424-1] << ", "<< model.boundary[710-1]<< endl;
	//cout << "BOUNDARY right(max)[R,G,B] = " << model.boundary[172-1+SIZE_BOUND/2] << ", "<< model.boundary[424-1+SIZE_BOUND/2] << ", "<< model.boundary[710-1+SIZE_BOUND/2]<< endl;

	f.close();
}

bool patternMatches(double hist[], struct JellyModel model){
	// adaboost with mode='apply'
	double * leftb=&model.boundary[0];
	double * rightb=&model.boundary[SIZE_BOUND/2];

//cout << "H=" << hist[172-1] << ", "<< hist[424-1] << ", "<< hist[710-1];
//cout << " vs. Max \t" << leftb[172-1] << ", "<< leftb[424-1] << ", "<< leftb[710-1];
//cout << " vs. Min \t" << rightb[172-1] << ", "<< rightb[424-1] << ", "<< rightb[710-1] << endl;

	for (int i=0; i<SIZE_BOUND/2; i++){
		if (hist[i] > rightb[i])
			hist[i]=rightb[i];
		if (hist[i] < leftb[i])
			hist[i]=leftb[i];
	}

//cout << "H max-min=" << hist[172-1] << ", "<< hist[424-1] << ", "<< hist[710-1];
//cout << " vs. \t" << model.threshold[0] << ", " << model.threshold[1] << ", " << model.threshold[2] << endl;
	/* Version escalar CORRECTA */
	double sum= 0.0;
	for (int c=0; c<3; c++){
			if (hist[model.dimension[c]-1] >= model.threshold[c])
				sum+=model.alpha[c];
			else
				sum-=model.alpha[c];
	}

	return (sum >= 0); // positive sign == match

	/* Version VECTOR
	std::vector<double> sum(SIZE_BOUND/2, 0.0);
	bool tots_positive=true;
	for (int i=0; i<SIZE_BOUND/2 && tots_positive; i++){
		for (int c=0; c<3; c++){
			if (hist[i] <= model.threshold[c])
				sum[i]=sum[i]-model.alpha[c];
			else
				sum[i]=sum[i]+model.alpha[c];
		}
		if (sum[i] < 0)
			tots_positive=false;
	}

	return (tots_positive); // positive sign == match
	*/
}

// pattern matching with a jellyfish model
void check_candidates(Mat a, list<HotSpot*>* jc){
	struct JellyModel model;
	readModel("JellyFish.txt", model);

	list<HotSpot*>::iterator it;
	for (it=jc->begin(); it != jc->end();){
		HotSpot * pc=*it;
		Rect bb=pc->GetBoundingBox();
		Mat medusa(bb.height, bb.width, a.type());
		medusa=a(Range(bb.y, bb.y+bb.height), Range(bb.x, bb.x+bb.width));
		//cout << "size candidate= "<< bb.height <<","<<bb.width<<endl;
		//if (bb.height>=5 && bb.width>=5){
		//	cout << "Jelly "; show_mat(medusa,5);
		//}

		double H[SIZE_BOUND/2];
		calculateH_feature(medusa,H);

		if (!patternMatches(H, model))
			it=jc->erase(it);
		else
			it++;

		//break;
	}
}

void paint_jellyfish(Mat& image, list<HotSpot*>* jc){
	list<HotSpot*>::iterator it;
	for (it=jc->begin(); it != jc->end(); it++){
		HotSpot * pc=*it;
		Rect bb=pc->GetBoundingBox();
		rectangle(image, bb, Scalar(0,0,255), 2);
	}
}

//////////////////////////////////////////////////////

string getBasename(string filename)
{
    size_t lastdot = filename.rfind(".");
    if (lastdot == string::npos) return filename;
    return filename.substr(0, lastdot);
}

string getRawBasename(string filename)
{
    size_t lastslash = filename.rfind("/");
    size_t lastdot = filename.rfind(".");

    if (lastslash == string::npos && lastdot == string::npos) return filename;
    else if (lastslash == string::npos) return filename.substr(0, lastdot);
    else if (lastdot == string::npos) return filename.substr(lastslash + 1, filename.length() - lastslash - 1);

    return filename.substr(lastslash + 1, lastdot - lastslash - 1);
}

string getExtension(string filename)
{
    size_t lastdot = filename.rfind(".");
    if (lastdot == string::npos || lastdot == filename.length() - 1 ) return "";
    return filename.substr(lastdot + 1, filename.length() - lastdot - 1);
}

string getPath(string filename)
{
    size_t lastslash = filename.rfind("/");
    if (lastslash == string::npos) return ".";
    return filename.substr(0, lastslash);
}
