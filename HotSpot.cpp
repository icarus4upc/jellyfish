/*
 * HotSpot.cpp
 *
 *  Created on: 17/06/2014
 *      Author: esther
 */

#include <iomanip>
#include "HotSpot.h"

int HotSpot::next_id = 1;

HotSpot::HotSpot(int column, int row, float temp)
{
    id = next_id++;
    pixels.push_back(Point(column, row));
    center = Point(column, row);
    boundingBox =  Rect(column, row, 1, 1);
    sum_xt = column * temp;
    sum_yt = row * temp;
    max_t = sum_t = temp;
}

HotSpot::~HotSpot()
{
	// TODO Auto-generated destructor stub
}

void HotSpot::AddPixel(int column, int row, float temp)
{
	pixels.push_back(Point(column, row));
	boundingBox |= Rect(column, row, 1, 1);
	max_t = (temp > max_t)? temp: max_t;
	sum_xt += column * temp;
	sum_yt += row * temp;
	sum_t += temp;
	center.x = (int)(sum_xt / sum_t);
	center.y = (int)(sum_yt / sum_t);
}

Rect HotSpot::GetBoundingBox() const
{
    return boundingBox;
}

void HotSpot::MergeHotSpot(HotSpot* h)
{
	pixels.splice(pixels.end(), h->pixels);
	boundingBox |= h->boundingBox;
	max_t = (h->max_t > max_t)? h->max_t: max_t;
	sum_xt += h->sum_xt;
	sum_yt += h->sum_yt;
	sum_t += h->sum_t;
	center.x = (int)(sum_xt / sum_t);
	center.y = (int)(sum_yt / sum_t);
}

ostream& operator<<(ostream &stream, const HotSpot &h)
{
    stream << "HS_" << h.id << endl;
    stream << "  NumPixels:   " << h.pixels.size() << endl;
    stream << "  Center:      " << h.center << endl;
    stream << "  Bounding:    " << h.boundingBox << endl;
    stream << fixed << setprecision(2);
    stream << "  MaxTemp:     " << h.max_t << " K" << endl;
    stream << "  AvgTemp:     " << h.sum_t/h.pixels.size() << " K" << endl;
    //stream << "  Magnitude:   " << setprecision(2) << h. magnitude << endl;
	return stream;
}
