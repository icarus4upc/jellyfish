/*
 * testMain.cpp
 *
 *  Created on: Jul 9, 2014
 *      Author: cristina
 */

#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "functions.hpp"

using namespace cv;
using namespace std;

int main_ExampleOf_Diag_Uses( int argc, char** argv )
{
  cout << "Welcome to OpenCV!!!" << endl;
  Mat a=(Mat_<float>(3,3) << 1, 2, 3, 4, 5, 6, 7, 8, 9);

  // use 1: extract main diagonal of a square matrix
  Mat d=a.diag(0);
  cout << "Diag(0)=" << d << endl;

  // use 2: creates a square matrix from a column matrix
  Mat newd=(Mat_<float>(1,3) << 7, 5, 1);
  Mat dd=Mat::diag(newd);
  cout << "Diag(7-7-7)=" << dd << endl;
  return 0;
}

int main_ExampleOf_sqrt_Uses( int argc, char** argv )
{
	cout << "Welcome to OpenCV!!!" << endl;
	Mat a=(Mat_<float>(3,1) << 1.0, 4.0, 3.0);
	Mat s(3,1,CV_32F);

	cv::sqrt(a, s);
	cout << "Sqrt(columnMatrix)=" << s << endl;
	return 0;
}

void test_functions (Mat a){
	cout << "A=" << a.channels() << "channels and " << a.size() << endl;
	Scalar M, S;
//	meanStdDev(a,M, S);
//	cout << "Mean=" << M << " Signa=" << S << endl;

	Mat n=Mat(a.rows*a.cols, 1, CV_8UC3, a.data).clone();
	cout << "Vector=" << n.channels() << "channels and " << n.size() << endl;
//	meanStdDev(r,M, S);
//	cout << "Mean=" << M << " Signa=" << S << endl;

	Mat t=a.clone();
	t.t();
	cout << "T=" << t.channels() << "channels and " << t.size() << endl;
//	meanStdDev(r,M, S);
//	cout << "Mean=" << M << " Signa=" << S << endl;

	Mat r(a.rows, a.cols, CV_8UC3);
	r.reshape(0,a.cols);
	cout << "Reshaped=" << r.channels() << "channels and " << r.size() << endl;
//	meanStdDev(r,M, S);
//	cout << "Mean=" << M << " Signa=" << S << endl;

	Mat bw;
	cvtColor( a, bw, CV_BGR2GRAY );
	cout << "bw=" << bw.channels() << " channels,  " << bw.rows << " rows, " << bw.cols << " cols" << endl;
//	meanStdDev(bw,M, S);
//	cout << "Mean=" << M << " Signa=" << S << endl;

	Mat b;
	a.convertTo(b, CV_64F, 1.0/255);
	// use 3dim MAT as 2dim => bits x 3colors
	cout << "B(64F)=" << b.channels() << "channels and " << b.size() << endl;
//	meanStdDev(b,M, S);
//	cout << "Mean=" << M << " Signa=" << S << endl;



	Mat Channels[3];
	split(b,Channels);
	cout << "SplitR=" << Channels[0].channels() << "channels and " << Channels[0].size() << endl;

//	Mat c(b.rows*3, b.cols*3, CV_64FC1);
//	int from_to[]={0,0, 1,0, 2,0};
//	mixChannels(&b, 1, &c, 1, from_to, 3);
//	cout << "C(mixed)=" << c.channels() << " size=" << c.size() << endl;
}
